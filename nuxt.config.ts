import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
css: [
    '@/assets/scss/core/index.scss',
    '@/assets/scss/core/_variables.scss',
    '@/assets/scss/core/_media.scss',
    '@/assets/scss/core/_colors.scss'
],
    modules: ['@pinia/nuxt', '@nuxtjs/strapi'],
    runtimeConfig: {
        strapi: {
            url: process.env.STRAPI_URL || 'http://localhost:1337'
        },
        public: {
            strapi: {
                url: 'http://localhost:1337'
            }
        }
    },
    strapi: {
        prefix: '/api'
    }
})
