const formatIngredients = (product: string | undefined): string[] => {
    return product.split(',').map(item => item.trim().charAt(0).toUpperCase() + item.trim().substring(1))
}
export default formatIngredients
