declare interface IPizza {
    id: number
    attributes: {
        name: string,
        ingredients: string,
        price: number,
        image: IImage
    }
}