import { defineStore } from 'pinia'

export const useAuthStore = defineStore({
    id: 'auth-store',
    state: () => {
        return {
            user: {},
            accessToken: ''
        }
    },
    actions: {
        setAccessToken (token: string)  {
            this.accessToken = token
        },
        setUser (userData)  {
            this.user = userData
        },
        reset() {
            this.accessToken = ''
            this.user = {}
        }
    },
    getters: {
        getAccessToken: state => state.accessToken,
        getUser: state => state.user
        // getMenuById: (state) => {
        //     return (pizzaId: number) => state.menu.find((pizza) => pizza.id === pizzaId)
        // },
    }
})