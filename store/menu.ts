import { defineStore } from 'pinia'

export const useMenuStore = defineStore({
    id: 'menu-store',
    state: () => {
        return {
            menu: [] as IPizza[],
        }
    },
    actions: {
        setMenu (result: IPizza[])  {
            this.menu = result
        }
    },
    getters: {
        getMenu: state => state.menu,
        getMenuSize: state => state.menu.length,
        getMenuById: (state) => {
            return (pizzaId: number) => state.menu.find((pizza) => pizza.id === pizzaId)
        },
    }
})