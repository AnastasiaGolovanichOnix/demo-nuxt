import {useAuthStore} from "~/store/auth";

const authStore = useAuthStore()
export default defineNuxtRouteMiddleware((to, from) => {
    if (!authStore.getAccessToken) {
        return navigateTo('/auth/login')
    }
})